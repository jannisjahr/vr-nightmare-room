import os
from datetime import datetime
import enum
from flask import Flask, render_template, jsonify, request, send_from_directory, redirect
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc
from werkzeug.exceptions import HTTPException, BadRequest
import random
import boto3

application = Flask(__name__)
application.config.from_object("config.Config")
# MODEL DEFINITION
db = SQLAlchemy(application)
s3 = boto3.client('s3', region_name='eu-central-1')

db.create_all()


masuda = [
    {
        'title': 'Sebastian Masuda',
        'description': 'This summer, Sebastian Masuda presents his first foray into the fascinating world of VR technology. His newest installation “Sense Share Bear“ opens up a completely new chapter of real life and digital experiences.  Created in conjunction with the Berlin based digital collective MORI, and an additional thank you to Special Special New York for providing the opportunity to contribute to the inaugural exhibition at the Museum of Art Pudong Shanghai, China.',
        'image': 'masuda.svg'
    },
    {
        'title': 'Sense Share Bear',
        'description': 'After founding the Kawaii Tribe community online, Sebastian teamed with MORI to conceive this piece of digital art completely remotely.  And thanks to advanced VR technology, our community as well as all others are able to join us online. Through the “Sense Share Bear,” Sebastian’s artistic vision of a digital tribe comes to life and celebrates its debut at the Museum of Art in Pudong.'
    },
    {
        'title': 'The Pandemic',
        'description': 'As the pandemic disconnected the world like nothing before and still impacts states, societies, families and friends attempting to recover from the damage that has been incurred, Sebastian has been searching for a new way to shape a digital society.  A new definition of belonging. And as we live in a digital age, we can consider ourselves as members of a digital tribe.'
    },
    {
        'title': 'New Art Forms',
        'description': 'Sebastian Masuda describes this work as follows: “The Sense Share Bear creates a new kind of communication between people who are far away from each other (and don\'t know each other) by decorating a virtual object together.  Communicating with a stranger in a strange country without words is not always easy. We can now explore ways of communicating in the new age via art.  In addition, with the disappearance of psychological distance, borders and walls through digital technology, we will embark upon a new form of creative communication with people from different cultures.'
    }
]

# define possible media type (video = gif)
class MediaType(enum.IntEnum):
    image = 0
    video = 1


# class for media files
class MediaFile(db.Model):
    __tablename__ = 'media'
    # Model for images
    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False)
    timestamp = db.Column(db.TIMESTAMP, nullable=False, default=datetime.utcnow)
    username = db.Column(db.String, nullable=True)
    filename = db.Column(db.String, nullable=True)
    filetype = db.Column(db.Enum(MediaType), nullable=False, default=MediaType.image)
    hidden = db.Column(db.Boolean, default=False)

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}


# exceptions
class WrongFormatException(HTTPException):
    code = 400
    description = "The given Format is not supported, json and html are supported"


class IDNotFound(HTTPException):
    code = 404
    description = "The given Image id was not found"

class Forbidden(HTTPException):
    code = 403
    description = "Forbidden"


class NoItems(HTTPException):
    code = 404
    description = "There are no Items right now"

# Yield successive n-sized
# chunks from l.
def divide_chunks(l, n):
    # looping till length l
    for i in range(0, len(l), n): 
        yield l[i:i + n]

# database connector
def get_media_files(page=0, per_page=50):
    return MediaFile.query.order_by(desc(MediaFile.timestamp)).paginate(page, per_page, False)


def get_media_file_by_id(media_id):
    return MediaFile.query.get(media_id)

def hide_show_media_file(media_id, hide=True):
    f = MediaFile.query.get(media_id)
    f.hidden = hide
    db.session.commit()
    return f


def store_media_file(file):
    media_file = MediaFile()
    db.session.add(media_file)
    db.session.commit()
    filename = "file%d.%s" % (media_file.id, file.filename.split('.')[-1].lower())
    try:
        s3.upload_fileobj(file, 'sensesharebear', filename)
    except:
        print("An exception occured...")
    media_file.filename = filename
    if filename.rsplit('.', 1)[1].lower() in application.config.get("ALLOWED_PHOTO_EXTENSIONS"):
        media_file.filetype = MediaType.image
    elif filename.rsplit('.', 1)[1].lower() in application.config.get("ALLOWED_VIDEO_EXTENSIONS"):
        media_file.filetype = MediaType.video
    db.session.commit()
    return media_file


def allowed_file(filename, key):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in application.config.get(key)


def authorize(request):
    if 'token' not in request.args:
        raise Forbidden
    if request.args['token'] != application.config['UPLOAD_TOKEN']:
        raise Forbidden

@application.route('/upload', methods=['POST'])
def upload_image():
    # Upload
    authorize(request)
    if 'file' not in request.files:
        raise BadRequest
    file = request.files['file']
    if file.filename == '':
        raise BadRequest
    if file and allowed_file(file.filename, "ALLOWED_PHOTO_EXTENSIONS") or \
            allowed_file(file.filename, "ALLOWED_VIDEO_EXTENSIONS"):
        f = store_media_file(file)
        return jsonify(id=f.id, type=f.filetype, timestamp=f.timestamp)
    else:
        raise BadRequest


@application.route('/<int:id_num>/hide', methods=['GET'])
def hide(id_num):
    authorize(request)
    hide_show_media_file(id_num)
    return redirect('/admin?token=%s#media-%s' %  (request.args['token'], str(id_num)))


@application.route('/<int:id_num>/show', methods=['GET'])
def show(id_num):
    authorize(request)
    hide_show_media_file(id_num, hide=False)
    return redirect('/admin?token=%s#media-%s' % (request.args['token'], str(id_num)))


@application.route('/',methods=['GET'])
def index():
    page = request.args.get("page", default=0)
    per_page = request.args.get("per_page", default=10000)
    request_format = request.args.get("format", "html")
    media_files = MediaFile.query.filter_by(hidden=False).all()
    if request_format == 'json':
        test = []
        limit = request.args.get("limit", default=1)
        rand = request.args.get("random", default=False)
        for u in MediaFile.query.all():
            test.append("/" + str(u.id) + "/file")
        if rand:
            random.shuffle(test)
        return jsonify(items=test[:int(limit)])
    elif request_format == 'html':
        myitems = list(divide_chunks(media_files, 6))
        print(len(myitems))
        return render_template("index.html", media_files=myitems, masuda=masuda)
    else:
        raise WrongFormatException

@application.route('/<int:id_num>')
def detail(id_num):
    media_file = get_media_file_by_id(id_num)
    if media_file is None:
        raise IDNotFound
    return render_template("detail.html", media_file=media_file)


@application.route('/admin')
def adminview():
    authorize(request)
    media_files = MediaFile.query.all()
    myitems = list(divide_chunks(media_files, 6))
    return render_template("index.html", media_files=myitems, token=request.args['token'], admin=True)


@application.route('/random')
def random_image():
    a = MediaFile.query.filter_by(hidden=False).all()
    if len(a) == 0:
        raise NoItems
    try:
        myurl = s3.generate_presigned_url('get_object', Params = {'Bucket': 'sensesharebear', 'Key': random.choice(a).filename}, ExpiresIn = 100)
    except Exception:
        return send_from_directory(".", "example.png")
    return redirect(myurl)

@application.route('/<int:id_num>/file')
def detail_file(id_num):
    media_file = get_media_file_by_id(id_num)
    if media_file is None:
        raise IDNotFound
    try:
        myurl = s3.generate_presigned_url('get_object', Params = {'Bucket': 'sensesharebear', 'Key': media_file.filename}, ExpiresIn = 100)
    except Exception:
        return send_from_directory(".", "example.png")
    return redirect(myurl)


@application.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('404.html'), 404


if __name__ == "__main__":
    application.debug = True
    application.run()
