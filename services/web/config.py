import os


basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://mori:h_NTcL-?6gxyBBkk@aa1d4nbmvi3sj2e.cntanhyfgpem.eu-central-1.rds.amazonaws.com/mori'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.getenv("SECRET_KEY")
    ALLOWED_PHOTO_EXTENSIONS = {'jpg', 'jpeg'}
    ALLOWED_VIDEO_EXTENSIONS = {'gif', 'mp4', 'webm'}
    UPLOAD_FOLDER = "~/bear-files/"
    UPLOAD_TOKEN = "932a66e3-6fe3-48da-a177-cf06092b875f"
